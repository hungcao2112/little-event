# Little Event

Welcome to Little Event App, the iOS application that can track your kid status


## App structure
Design pattern: MVVM (with RxSwift)
- **Reason**: This is an interesting design pattern for iOS application, I haven't use it for a long time and I just want to give it a chance in this assignment

- **Advantages**: Using this design pattern could make the project more "reactive" by the bindings of model and view. The separate of Model-View-ViewModel avoid Model from expose itself to View. Make the project easier to test, maintaince and extend

    With the strongly developing of SwiftUI and Combine, can say that MVVM is the future of iOS development

- **Disadvantages**: Not very popular with mostly iOS developer (compared with Android) and pretty hard to understand for beginners.

    With components are handled separately, the amount of code you need to implement a simple function may become huge and complicated. Also, the bindings sometime caused painful problems


## Getting started
1. Pull/Download the repository
2. Install CocoaPods (if it was installed, skip this step)
2. Run `pod install`
3. Launch the app in Simulator or device

## Pods
[RxSwift](https://github.com/ReactiveX/RxSwift)\
Main framework for the application to build the MVVM design pattern

[RxKingfisher](https://github.com/RxSwiftCommunity/RxKingfisher)\
Kingfisher-the library for downloading and caching images URLs and its Rx extensions

[ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper)\
A library that helps to convert model objects (classes or structs) to and from JSON

[R.swift](https://github.com/mac-cain13/R.swift)\
Get strong typed, autocompleted resources like images, fonts and segues in Swift projects

