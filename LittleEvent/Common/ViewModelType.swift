//
//  ViewModelType.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}

class ViewModel: NSObject {
    let provider: MockDataProvider
    
    init(provider: MockDataProvider) {
        self.provider = provider
        super.init()
    }
    
    deinit {
        logDebug("\(type(of: self)): Deinited")
    }
}
