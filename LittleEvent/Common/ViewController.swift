//
//  ViewController.swift
//  LittleEvent
//
//  Created by Hung Cao on 28/12/2022.
//

import Foundation
import RxSwift
import RxCocoa

class ViewController: UIViewController, Navigatable {
    
    var viewModel: ViewModel!
    var navigator: Navigator!
    
    init(viewModel: ViewModel, navigator: Navigator) {
        self.viewModel = viewModel
        self.navigator = navigator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(nibName: nil, bundle: nil)
    }
    
    deinit {
        logDebug("\(type(of: self)): Deinited")
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        logDebug("\(type(of: self)): Received Memory Warning")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
        bindViewModel()
    }
    
    func bindViewModel() {}
    
    func setupUI() {}
    
}
