//
//  MockData.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

class MockData: MockDataProvider {
    func getTimeline() -> Single<[Timeline]> {
        do {
            if let bundlePath = Bundle.main.path(
                forResource: "user_stories",
                ofType: "json"
            ), let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    let dataDict = jsonDict["data"] as! [String: Any]
                    let userTimelineDict = dataDict["userTimeline"] as! [[String: Any]]
                    let timelines = Mapper<Timeline>().mapArray(JSONArray: userTimelineDict)
                    return Single.just(timelines)
                } catch {
                    logError(error.localizedDescription)
                }
            }
        } catch {
            logError(error.localizedDescription)
        }
        return Single.just([])
    }
}
