//
//  MockData.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol MockDataProvider {
    func getTimeline() -> Single<[Timeline]>
}
