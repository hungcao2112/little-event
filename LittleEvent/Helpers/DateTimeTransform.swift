//
//  DateTimeTransform.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import Foundation
import ObjectMapper

open class DateTimeTransform: DateFormatterTransform {
    
    init() {
        let formatter = DateFormatter(withFormat: "YYYY-MM-dd'T'HH:mm:ss", locale: Locale.current.identifier)
        super.init(dateFormatter: formatter)
    }
}
