//
//  EventTypeTransform.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import Foundation
import ObjectMapper

open class EventTypeTransform: TransformType {
    public typealias Object = EventType
    public typealias JSON = String
    
    public func transformFromJSON(_ value: Any?) -> EventType? {
        guard let value = value as? String else {
            return nil
        }
        switch value {
        case "event":
            return .event
        case "everydayHealth":
            return .everydayHealth
        case "checkIn":
            return .checkIn
        case "checkOut":
            return .checkOut
        case "portfolio":
            return .portfolio
        case "story_exported":
            return .storyExported
        case "story_published":
            return .storyPublished
        default:
            return nil
        }
    }
    
    public func transformToJSON(_ value: EventType?) -> String? {
        return nil
    }
}
