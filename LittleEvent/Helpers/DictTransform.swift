//
//  DictTransform.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import Foundation
import ObjectMapper

open class DictTransform: TransformType {
    public typealias Object = [String: Any]
    public typealias JSON = String
    
    public func transformFromJSON(_ value: Any?) -> [String : Any]? {
        if let jsonData = (value as? String)?.data(using: .utf8) {
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                return jsonDict
            } catch {
                logError(error.localizedDescription)
            }
        }
        return nil
    }
    
    public func transformToJSON(_ value: [String : Any]?) -> String? {
        return "\(value ?? [:])"
    }
}
