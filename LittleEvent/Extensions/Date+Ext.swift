//
//  Date+Ext.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import Foundation

extension Date {
    func toString(format: String = "dd MMM yyyy - hh:mm:ss a") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
