//
//  Timeline.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import ObjectMapper
import RxDataSources

struct Timeline: Mappable {
    var typeName: String?
    var eventDate: Date?
    var eventDescription: String?
    var eventSnapshot: [String: Any]?
    var eventType: EventType?
    var insertedAt: Date?
    
    var imageUrl: String? {
        guard let eventType = eventType else { return nil }
        switch eventType {
        case .checkOut:
            return eventSnapshot?["checkinUrl"] as? String
        case .checkIn:
            return eventSnapshot?["checkinUrl"] as? String
        case .portfolio:
            return eventSnapshot?["imageUrl"] as? String
        case .storyPublished:
            return eventSnapshot?["story_image"] as? String
        default:
            return nil
        }
    }
    
    init?(map: ObjectMapper.Map) {}
    
    init() {}
    
    mutating func mapping(map: ObjectMapper.Map) {
        typeName <- map["__typename"]
        eventDate <- (map["eventDate"], DateTimeTransform())
        eventDescription <- map["eventDescription"]
        eventSnapshot <- (map["eventSnapshot"], DictTransform())
        eventType <- (map["eventType"], EventTypeTransform())
        insertedAt <- (map["insertedAt"], DateTimeTransform())
    }
}

struct TimelineSection {
    var date: Date
    var items: [Item]
}

extension TimelineSection: SectionModelType {
    typealias Item = Timeline
    
    init(original: TimelineSection, items: [Item]) {
        self = original
        self.items = items
    }
}
