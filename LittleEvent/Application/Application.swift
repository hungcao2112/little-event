//
//  Application.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import UIKit

final class Application: NSObject {
    static let shared = Application()
    
    var window: UIWindow?
    
    let navigator: Navigator
    
    var provider: MockDataProvider?
    
    private override init() {
        navigator = Navigator.default
        provider = MockData()
        super.init()
    }
    
    func presentInitialScreen(in window: UIWindow?) {
        guard let window = window, let provider = provider else { return }
        self.window = window
        let viewModel = HomeTabBarViewModel(provider: provider)
        self.navigator.show(segue: .tabs(viewModel: viewModel), sender: nil, transition: .root(in: window))
    }
}
