//
//  Navigator.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol Navigatable {
    var navigator: Navigator! { get set }
}

class Navigator {
    static var `default` = Navigator()
    
    // MARK: - segues list, all app scenes
    enum Scene {
        case tabs(viewModel: HomeTabBarViewModel)
        case detail(ViewModel: DetailViewModel)
    }
    
    enum Transition {
        case root(in: UIWindow)
        case navigation
    }
    
    // MARK: - get a single VC
    func get(segue: Scene) -> UIViewController? {
        switch segue {
        case .tabs(let viewModel):
            let rootVC = HomeTabBarController(viewModel: viewModel, navigator: self)
            return rootVC
        case .detail(let viewModel):
            let detailVC = DetailViewController(viewModel: viewModel, navigator: self)
            return detailVC
        }
    }
    
    // MARK: - invoke a single segue
    func show(segue: Scene, sender: UIViewController?, transition: Transition = .navigation) {
        if let target = get(segue: segue) {
            show(target: target, sender: sender, transition: transition)
        }
    }
    
    private func show(target: UIViewController, sender: UIViewController?, transition: Transition) {
        switch transition {
        case .root(in: let window):
            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                window.rootViewController = target
            }, completion: nil)
            return
        default: break
        }
        guard let sender = sender else {
            fatalError("You need to pass in a sender for .navigation or .modal transitions")
        }

        if let nav = sender as? UINavigationController {
            // push root controller on navigation stack
            target.hidesBottomBarWhenPushed = true
            nav.pushViewController(target, animated: false)
            return
        }
        switch transition {
        case .navigation:
            if let nav = sender.navigationController {
                // push controller to navigation stack
                target.hidesBottomBarWhenPushed = true
                nav.pushViewController(target, animated: true)
            }
        default:
            break
        }
    }
}
