//
//  FeedViewController.swift
//  LittleEvent
//
//  Created by Hung Cao on 28/12/2022.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit
import RxDataSources

class FeedViewController: ViewController {
    lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .plain)
        view.rowHeight = UITableView.automaticDimension
        view.estimatedRowHeight = 50
        view.tableFooterView = UIView()
        view.sectionHeaderTopPadding = 0.0
        view.rx.setDelegate(self).disposed(by: rx.disposeBag)
        return view
    }()
    
    lazy var shareFeedbackButton: UIButton = {
        var configuration = UIButton.Configuration.plain()
        configuration.title = "Share Feedback"
        configuration.titlePadding = 8
        configuration.baseForegroundColor = .systemGray2
        configuration.baseBackgroundColor = .clear
        configuration.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
           var outgoing = incoming
            outgoing.font = .systemFont(ofSize: 14)
           return outgoing
        }
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.borderColor = .systemGray2
        button.borderWidth = 1
        button.cornerRadius = 14
        return button
    }()
    
    lazy var profileBarButtonItem = UIBarButtonItem(
        image: UIImage(systemName: "person.crop.circle"),
        style: .plain,
        target: self,
        action: nil
    )
    
    lazy var scanBarButtonItem = UIBarButtonItem(
        image: UIImage(systemName: "qrcode.viewfinder"),
        style: .plain,
        target: self,
        action: nil
    )
    
    lazy var feedbackBarButtonItem = UIBarButtonItem(customView: shareFeedbackButton)
    
    var dataSource: RxTableViewSectionedReloadDataSource<TimelineSection>?
    
    override func setupUI() {
        super.setupUI()
        
        profileBarButtonItem.tintColor = .systemGray2
        scanBarButtonItem.tintColor = .systemGray2
        navigationItem.leftBarButtonItems = [profileBarButtonItem]
        navigationItem.rightBarButtonItems = [scanBarButtonItem, feedbackBarButtonItem]
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        tableView.register(UINib(resource: R.nib.feedCell), forCellReuseIdentifier: R.reuseIdentifier.feedCell.identifier)
        
        tableView.rx.itemSelected.asDriver().drive(onNext: { [weak self] _ in
            guard let viewModel = self?.viewModel else {
                return
            }
            self?.navigator.show(segue: .detail(ViewModel: DetailViewModel(provider: viewModel.provider)), sender: self)
        }).disposed(by: rx.disposeBag)
    }
    
    override func bindViewModel() {
        super.bindViewModel()
        
        guard let viewModel = viewModel as? FeedViewModel else {
            return
        }
        
        let input = FeedViewModel.Input()
        let output = viewModel.transform(input: input)
        
        dataSource = RxTableViewSectionedReloadDataSource<TimelineSection> { dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.feedCell, for: indexPath)!
            cell.timeline = item
            return cell
        }
        
        output.items.asDriver().drive(tableView.rx.items(dataSource: dataSource!)).disposed(by: rx.disposeBag)
    }
}

extension FeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: .zero)
        view.backgroundColor = .systemGray4
        
        let titleLabel = UILabel(frame: .zero)
        titleLabel.textColor = .gray
        titleLabel.font = .systemFont(ofSize: 14)
        view.addSubview(titleLabel)
        titleLabel.text = dataSource?[section].date.toString(format: "EEE, MMM dd, yyyy")
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(56)
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(16)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
}
