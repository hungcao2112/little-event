//
//  FeedCell.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import UIKit
import Kingfisher

class FeedCell: UITableViewCell {

    @IBOutlet weak var topContentStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var snapshotImageView: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    
    var timeline: Timeline? {
        didSet {
            updateUI()
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        topContentStackView.spacing = 16
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        actionButton.configuration?.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
            var outgoing = incoming
            outgoing.font = UIFont.systemFont(ofSize: 14)
            return outgoing
        }
        actionButton.setPreferredSymbolConfiguration(UIImage.SymbolConfiguration(pointSize: 12), forImageIn: .normal)
    }
    
    private func updateUI() {
        titleLabel.text = timeline?.eventType?.title
        descriptionLabel.text = timeline?.eventDescription ?? " "
        iconImageView.image = timeline?.eventType?.icon
        buttonView.isHidden = !(timeline?.eventType == .storyExported || timeline?.eventType == .event)
        actionButton.setTitle(timeline?.eventType?.buttonTitle, for: .normal)
        actionButton.setImage(timeline?.eventType?.buttonImage, for: .normal)
        if let imageUrl = timeline?.imageUrl {
            snapshotImageView.isHidden = false
            snapshotImageView.kf.setImage(with: URL(string: imageUrl))
        }
        else {
            snapshotImageView.isHidden = true
        }
    }
}
