//
//  FeedViewModel.swift
//  LittleEvent
//
//  Created by Hung Cao on 28/12/2022.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

public enum EventType {
    case event, everydayHealth, checkOut, checkIn, portfolio, storyExported, storyPublished
    
    var title: String {
        switch self {
        case .event:
            return "EVENTS"
        case .everydayHealth:
            return "EVERYDAY HEALTH"
        case .checkOut, .checkIn:
            return "CHECK IN & OUT"
        case .portfolio, .storyExported, .storyPublished:
            return "PORTFOLIO"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .event:
            return UIImage(systemName: "calendar")
        case .everydayHealth:
            return UIImage(systemName: "heart.fill")
        case .checkOut, .checkIn:
            return UIImage(systemName: "person.fill.checkmark")
        case .portfolio, .storyExported, .storyPublished:
            return UIImage(systemName: "folder.fill")
        }
    }
    
    var buttonTitle: String {
        switch self {
        case .event:
            return "Add"
        case .storyExported:
            return "Download"
        default:
            return ""
        }
    }
    
    var buttonImage: UIImage? {
        switch self {
        case .event:
            return UIImage(systemName: "calendar.badge.plus")
        case .storyExported:
            return UIImage(systemName: "arrow.down.to.line")
        default:
            return nil
        }
    }
}

class FeedViewModel: ViewModel, ViewModelType {
    struct Input {}
    
    struct Output {
        let items: BehaviorRelay<[TimelineSection]>
    }
    
    func transform(input: Input) -> Output {
        
        let items = BehaviorRelay<[TimelineSection]>(value: [])
        
        provider.getTimeline().subscribe { [weak self] in
            items.accept(self?.groupByDate(items: $0) ?? [])
        }.disposed(by: rx.disposeBag)
        
        return Output(items: items)
    }
    
    private func groupByDate(items: [Timeline]) -> [TimelineSection] {
        let groupDict = Dictionary(grouping: items) { timeline -> DateComponents in
            let date = Calendar.current.dateComponents([.day, .year, .month], from: (timeline.eventDate) ?? Date())
            return date
        }
        var sections: [TimelineSection] = []
        for (date, items) in groupDict {
            sections.append(TimelineSection(date: Calendar.current.date(from: date)!, items: items))
        }
        sections = sections.sorted {
            $0.date > $1.date
        }
        return sections
    }
}
