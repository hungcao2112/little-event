//
//  HomeTabBarViewModel.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import RxCocoa
import RxSwift

class HomeTabBarViewModel: ViewModel, ViewModelType {
    struct Input {
        
    }
    
    struct Output {
        let tabBarItems: Driver<[HomeTabBarItem]>
    }
    
    func transform(input: Input) -> Output {
        let tabBarItems = Observable<[HomeTabBarItem]>.just([
            .news, .checkIn, .inbox, .portfolio, .more
        ]).asDriver(onErrorJustReturn: [])
        
        return Output(tabBarItems: tabBarItems)
    }
    
    func viewModel(for tabBarItem: HomeTabBarItem) -> ViewModel {
        switch tabBarItem {
        case .news:
            let viewModel = FeedViewModel(provider: provider)
            return viewModel
        case .checkIn:
            let viewModel = ViewModel(provider: provider)
            return viewModel
        case .inbox:
            let viewModel = ViewModel(provider: provider)
            return viewModel
        case .portfolio:
            let viewModel = ViewModel(provider: provider)
            return viewModel
        case .more:
            let viewModel = ViewModel(provider: provider)
            return viewModel
        }
    }
}
