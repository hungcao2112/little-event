//
//  HomeTabBarViewController.swift
//  LittleEvent
//
//  Created by Hung Cao on 27/12/2022.
//

import Foundation
import RxSwift
import NSObject_Rx

enum HomeTabBarItem: Int {
    case news, checkIn, inbox, portfolio, more
    
    private func controller(with viewModel: ViewModel, navigator: Navigator) -> UIViewController {
        switch self {
        case .news:
            let viewController = FeedViewController(viewModel: viewModel, navigator: navigator)
            return UINavigationController(rootViewController: viewController)
        case .checkIn:
            let viewController = ViewController(viewModel: viewModel, navigator: navigator)
            return UINavigationController(rootViewController: viewController)
        case .inbox:
            let viewController = ViewController(viewModel: viewModel, navigator: navigator)
            return UINavigationController(rootViewController: viewController)
        case .portfolio:
            let viewController = ViewController(viewModel: viewModel, navigator: navigator)
            return UINavigationController(rootViewController: viewController)
        case .more:
            let viewController = ViewController(viewModel: viewModel, navigator: navigator)
            return UINavigationController(rootViewController: viewController)
        }
    }
    
    var image: UIImage? {
        switch self {
        case .news:
            return UIImage(systemName: "list.dash.header.rectangle")
        case .checkIn:
            return UIImage(systemName: "person.badge.shield.checkmark")
        case .inbox:
            return UIImage(systemName: "text.bubble")
        case .portfolio:
            return UIImage(systemName: "folder")
        case .more:
            return UIImage(systemName: "ellipsis")
        }
    }
    
    var title: String {
        switch self {
        case .news:
            return "News"
        case .checkIn:
            return "Check in"
        case .inbox:
            return "Inbox"
        case .portfolio:
            return "Portfolio"
        case .more:
            return "More"
        }
    }
    
    func getController(with viewModel: ViewModel, navigator: Navigator) -> UIViewController {
        let vc = controller(with: viewModel, navigator: navigator)
        let item = UITabBarItem(title: title, image: image, selectedImage: image?.withTintColor(.systemCyan, renderingMode: .alwaysOriginal))
        vc.tabBarItem = item
        return vc
    }
}

class HomeTabBarController: UITabBarController, Navigatable {
    var navigator: Navigator!
    var viewModel: HomeTabBarViewModel!
    
    init(viewModel: HomeTabBarViewModel, navigator: Navigator) {
        self.viewModel = viewModel
        self.navigator = navigator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.backgroundColor = .white
        self.tabBar.barTintColor = .white
        bindViewModel()
    }
    
    func bindViewModel() {
        let input = HomeTabBarViewModel.Input()
        let output = viewModel.transform(input: input)
        
        output.tabBarItems.delay(.milliseconds(50)).drive { [weak self] tabBarItems in
            guard let `self` = self else { return }
            let controllers = tabBarItems.map { $0.getController(with: self.viewModel.viewModel(for: $0), navigator: self.navigator) }
            self.setViewControllers(controllers, animated: false)
        }.disposed(by: rx.disposeBag)
    }
}
