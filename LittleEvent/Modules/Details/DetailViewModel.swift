//
//  DetailViewModel.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class DetailViewModel: ViewModel, ViewModelType {
    struct Input {}
    
    struct Output {}
    
    func transform(input: Input) -> Output {
        return Output()
    }
}
