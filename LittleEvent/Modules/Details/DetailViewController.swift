//
//  DetailViewController.swift
//  LittleEvent
//
//  Created by Hung Cao on 29/12/2022.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class DetailViewController: ViewController {
    
    override func setupUI() {
        super.setupUI()
        title = "Details"
    }
}
